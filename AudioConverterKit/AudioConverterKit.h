//
//  AudioConverterKit.h
//  AudioConverterKit
//
//  Created by Lennon He on 27/10/2017.
//  Copyright © 2017 Lennon He. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AudioConverterKit.
FOUNDATION_EXPORT double AudioConverterKitVersionNumber;

//! Project version string for AudioConverterKit.
FOUNDATION_EXPORT const unsigned char AudioConverterKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AudioConverterKit/PublicHeader.h>
#import <AudioConverterKit/ExtAudioConverter.h>


